﻿using NSubstitute;
using NUnit.Framework;
using UnityEngine;

namespace Snake3D.Test
{
    [TestFixture]
    public class MovementControllerTest
    {
        [Test]
        public void NoMovement()
        {
            var input = Substitute.For<IInputController>();
            var snake = new MovementController(input);
            input.GetInput().Returns(IntVector2.Zero);
            snake.ProcessMovement();
            Assert.That(snake.Position.Equals(new IntVector3(0, 0, 1)));
            Assert.That(snake.Rotation.ApproxEqual(Quaternion.LookRotation(Vector3.forward, Vector3.up)));
        }

        [Test]
        public void UpwardMovement()
        {
            var input = Substitute.For<IInputController>();
            var snake = new MovementController(input);
            input.GetInput().Returns(IntVector2.Up);
            snake.ProcessMovement();
            Assert.That(snake.Position.Equals(new IntVector3(0, 1, 0)));
            Assert.That(snake.Rotation.ApproxEqual(Quaternion.LookRotation(Vector3.up, Vector3.back)));
        }

        [Test]
        public void DownwardMovement()
        {
            var input = Substitute.For<IInputController>();
            var snake = new MovementController(input);
            input.GetInput().Returns(IntVector2.Down);
            snake.ProcessMovement();
            Assert.That(snake.Position.Equals(new IntVector3(0, -1, 0)));
            Assert.That(snake.Rotation.ApproxEqual(Quaternion.LookRotation(Vector3.down, Vector3.forward)));
        }

        [Test]
        public void LeftMovement()
        {
            var input = Substitute.For<IInputController>();
            var snake = new MovementController(input);
            input.GetInput().Returns(IntVector2.Left);
            snake.ProcessMovement();
            Assert.That(snake.Position.Equals(new IntVector3(-1, 0, 0)));
            Assert.That(snake.Rotation.ApproxEqual(Quaternion.LookRotation(Vector3.left, Vector3.up)));
        }

        [Test]
        public void RightMovement()
        {
            var input = Substitute.For<IInputController>();
            var snake = new MovementController(input);
            input.GetInput().Returns(IntVector2.Right);
            snake.ProcessMovement();
            Assert.That(snake.Position.Equals(new IntVector3(1, 0, 0)));
            Assert.That(snake.Rotation.ApproxEqual(Quaternion.LookRotation(Vector3.right, Vector3.up)));
        }

        [Test]
        public void RightNoMovement()
        {
            var input = Substitute.For<IInputController>();
            var snake = new MovementController(input);
            input.GetInput().Returns(IntVector2.Right, IntVector2.Zero);
            snake.ProcessMovement();
            snake.ProcessMovement();
            Assert.That(snake.Position.Equals(new IntVector3(2, 0, 0)));
            Assert.That(snake.Rotation.ApproxEqual(Quaternion.LookRotation(Vector3.right, Vector3.up)));
        }

        [Test]
        public void RightRightMovement()
        {
            var input = Substitute.For<IInputController>();
            var snake = new MovementController(input);
            input.GetInput().Returns(IntVector2.Right, IntVector2.Right);
            snake.ProcessMovement();
            snake.ProcessMovement();
            Assert.That(snake.Position.Equals(new IntVector3(1, 0, -1)));
            Assert.That(snake.Rotation.ApproxEqual(Quaternion.LookRotation(Vector3.back, Vector3.up)));
        }

        [Test]
        public void RightLeftMovement()
        {
            var input = Substitute.For<IInputController>();
            var snake = new MovementController(input);
            input.GetInput().Returns(IntVector2.Right, IntVector2.Left);
            snake.ProcessMovement();
            snake.ProcessMovement();
            Assert.That(snake.Position.Equals(new IntVector3(1, 0, 1)));
            Assert.That(snake.Rotation.ApproxEqual(Quaternion.LookRotation(Vector3.forward, Vector3.up)));
        }

        [Test]
        public void RightUpMovement()
        {
            var input = Substitute.For<IInputController>();
            var snake = new MovementController(input);
            input.GetInput().Returns(IntVector2.Right, IntVector2.Up);
            snake.ProcessMovement();
            snake.ProcessMovement();
            Assert.That(snake.Position.Equals(new IntVector3(1, 1, 0)));
            Assert.That(snake.Rotation.ApproxEqual(Quaternion.LookRotation(Vector3.up, Vector3.left)));
        }

        [Test]
        public void RightDownMovement()
        {
            var input = Substitute.For<IInputController>();
            var snake = new MovementController(input);
            input.GetInput().Returns(IntVector2.Right, IntVector2.Down);
            snake.ProcessMovement();
            snake.ProcessMovement();
            Assert.That(snake.Position.Equals(new IntVector3(1, -1, 0)));
            Assert.That(snake.Rotation.ApproxEqual(Quaternion.LookRotation(Vector3.down, Vector3.right)));
        }

        [Test]
        public void UpNoMovement()
        {
            var input = Substitute.For<IInputController>();
            var snake = new MovementController(input);
            input.GetInput().Returns(IntVector2.Up, IntVector2.Zero);
            snake.ProcessMovement();
            snake.ProcessMovement();
            Assert.That(snake.Position.Equals(new IntVector3(0, 2, 0)));
            Assert.That(snake.Rotation.ApproxEqual(Quaternion.LookRotation(Vector3.up, Vector3.back)));
        }

        [Test]
        public void UpRightMovement()
        {
            var input = Substitute.For<IInputController>();
            var snake = new MovementController(input);
            input.GetInput().Returns(IntVector2.Up, IntVector2.Right);
            snake.ProcessMovement();
            snake.ProcessMovement();
            Assert.That(snake.Position.Equals(new IntVector3(1, 1, 0)));
            Assert.That(snake.Rotation.ApproxEqual(Quaternion.LookRotation(Vector3.right, Vector3.back)));
        }

        [Test]
        public void UpLeftMovement()
        {
            var input = Substitute.For<IInputController>();
            var snake = new MovementController(input);
            input.GetInput().Returns(IntVector2.Up, IntVector2.Left);
            snake.ProcessMovement();
            snake.ProcessMovement();
            Assert.That(snake.Position.Equals(new IntVector3(-1, 1, 0)));
            Assert.That(snake.Rotation.ApproxEqual(Quaternion.LookRotation(Vector3.left, Vector3.back)));
        }

        [Test]
        public void UpUpMovement()
        {
            var input = Substitute.For<IInputController>();
            var snake = new MovementController(input);
            input.GetInput().Returns(IntVector2.Up, IntVector2.Up);
            snake.ProcessMovement();
            snake.ProcessMovement();
            Assert.That(snake.Position.Equals(new IntVector3(0, 1, -1)));
            Assert.That(snake.Rotation.ApproxEqual(Quaternion.LookRotation(Vector3.back, Vector3.down)));
        }

        [Test]
        public void UpDownMovement()
        {
            var input = Substitute.For<IInputController>();
            var snake = new MovementController(input);
            input.GetInput().Returns(IntVector2.Up, IntVector2.Down);
            snake.ProcessMovement();
            snake.ProcessMovement();
            Assert.That(snake.Position.Equals(new IntVector3(0, 1, 1)));
            Assert.That(snake.Rotation.ApproxEqual(Quaternion.LookRotation(Vector3.forward, Vector3.up)));
        }

        [Test]
        public void DownNoMovement()
        {
            var input = Substitute.For<IInputController>();
            var snake = new MovementController(input);
            input.GetInput().Returns(IntVector2.Down, IntVector2.Zero);
            snake.ProcessMovement();
            snake.ProcessMovement();
            Assert.That(snake.Position.Equals(new IntVector3(0, -2, 0)));
            Assert.That(snake.Rotation.ApproxEqual(Quaternion.LookRotation(Vector3.down, Vector3.forward)));
        }

        [Test]
        public void DownRightMovement()
        {
            var input = Substitute.For<IInputController>();
            var snake = new MovementController(input);
            input.GetInput().Returns(IntVector2.Down, IntVector2.Right);
            snake.ProcessMovement();
            snake.ProcessMovement();
            Assert.That(snake.Position.Equals(new IntVector3(1, -1, 0)));
            Assert.That(snake.Rotation.ApproxEqual(Quaternion.LookRotation(Vector3.right, Vector3.forward)));
        }

        [Test]
        public void DownLeftMovement()
        {
            var input = Substitute.For<IInputController>();
            var snake = new MovementController(input);
            input.GetInput().Returns(IntVector2.Down, IntVector2.Left);
            snake.ProcessMovement();
            snake.ProcessMovement();
            Assert.That(snake.Position.Equals(new IntVector3(-1, -1, 0)));
            Assert.That(snake.Rotation.ApproxEqual(Quaternion.LookRotation(Vector3.left, Vector3.forward)));
        }

        [Test]
        public void DownUpMovement()
        {
            var input = Substitute.For<IInputController>();
            var snake = new MovementController(input);
            input.GetInput().Returns(IntVector2.Down, IntVector2.Up);
            snake.ProcessMovement();
            snake.ProcessMovement();
            Assert.That(snake.Position.Equals(new IntVector3(0, -1, 1)));
            Assert.That(snake.Rotation.ApproxEqual(Quaternion.LookRotation(Vector3.forward, Vector3.up)));
        }

        [Test]
        public void DownDownMovement()
        {
            var input = Substitute.For<IInputController>();
            var snake = new MovementController(input);
            input.GetInput().Returns(IntVector2.Down, IntVector2.Down);
            snake.ProcessMovement();
            snake.ProcessMovement();
            Assert.That(snake.Position.Equals(new IntVector3(0, -1, -1)));
            Assert.That(snake.Rotation.ApproxEqual(Quaternion.LookRotation(Vector3.back, Vector3.down)));
        }
    }

    [TestFixture]
    public class SnakeController
    {
        [Test]
        public void AddBody([NUnit.Framework.Range(1, 5)] int numBoodyParts)
        {
            IInputController inputController;
            IBody[] parts;
            CreateSnake(numBoodyParts, out inputController, out parts);
            for (int i = 0; i < numBoodyParts; i++)
            {
                parts[i].Received().SetPosition(new IntVector3(0, 0, -1 * (i + 1)), Arg.Any<Quaternion>());
            }
        }

        [Test]
        public void MoveForward()
        {
            IInputController inputController;
            IBody[] parts;
            var snake = CreateSnake(2, out inputController, out parts);
            inputController.GetInput().Returns(IntVector2.Zero);
            parts[0].ClearReceivedCalls();
            parts[1].ClearReceivedCalls();

            snake.ProcessMovement();
            snake.ProcessMovement();
            parts[0].Received().SetPosition(new IntVector3(0, 0, 0), Arg.Any<Quaternion>());
            parts[0].Received().SetPosition(new IntVector3(0, 0, 1), Arg.Any<Quaternion>());
            parts[1].Received().SetPosition(new IntVector3(0, 0, -1), Arg.Any<Quaternion>());
            parts[1].Received().SetPosition(new IntVector3(0, 0, 0), Arg.Any<Quaternion>());
        }

        [Test]
        public void MoveLeft()
        {
            IInputController inputController;
            IBody[] parts;
            var snake = CreateSnake(2, out inputController, out parts);
            inputController.GetInput().Returns(IntVector2.Left, IntVector2.Zero);
            parts[0].ClearReceivedCalls();
            parts[1].ClearReceivedCalls();

            snake.ProcessMovement();
            snake.ProcessMovement();
            parts[0].Received().SetPosition(new IntVector3(0, 0, 0), Arg.Any<Quaternion>());
            parts[0].Received().SetPosition(new IntVector3(-1, 0, 0), Arg.Any<Quaternion>());
            parts[1].Received().SetPosition(new IntVector3(0, 0, -1), Arg.Any<Quaternion>());
            parts[1].Received().SetPosition(new IntVector3(0, 0, 0), Arg.Any<Quaternion>());
        }

        [Test]
        public void MoveLeftRight()
        {
            IInputController inputController;
            IBody[] parts;
            var snake = CreateSnake(2, out inputController, out parts);
            inputController.GetInput().Returns(IntVector2.Left, IntVector2.Right, IntVector2.Zero);
            parts[0].ClearReceivedCalls();
            parts[1].ClearReceivedCalls();

            snake.ProcessMovement();
            snake.ProcessMovement();
            snake.ProcessMovement();
            parts[0].Received().SetPosition(new IntVector3(0, 0, 0), Arg.Any<Quaternion>());
            parts[0].Received().SetPosition(new IntVector3(-1, 0, 0), Arg.Any<Quaternion>());
            parts[0].Received().SetPosition(new IntVector3(-1, 0, 1), Arg.Any<Quaternion>());
            parts[1].Received().SetPosition(new IntVector3(0, 0, -1), Arg.Any<Quaternion>());
            parts[1].Received().SetPosition(new IntVector3(0, 0, 0), Arg.Any<Quaternion>());
            parts[1].Received().SetPosition(new IntVector3(-1, 0, 0), Arg.Any<Quaternion>());
        }

        private static Snake CreateSnake(int bodyParts, out IInputController inputController, out IBody[] parts)
        {
            IHead head;
            return CreateSnake(bodyParts, out inputController, out parts, out head);
        }

        private static Snake CreateSnake(int bodyParts, out IInputController inputController, out IBody[] parts, out IHead head)
        {
            parts = new IBody[bodyParts];
            head = Substitute.For<IHead>();
            inputController = Substitute.For<IInputController>();
            var snake = new Snake(head, inputController, Substitute.For<IGameManager>());
            for (int i = 0; i < bodyParts; i++)
            {
                parts[i] = Substitute.For<IBody>();
                snake.AddBodyPart(parts[i]);
            }
            return snake;
        }
    }
}
