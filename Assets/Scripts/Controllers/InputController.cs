﻿using JetBrains.Annotations;
using UnityEngine;

namespace Snake3D
{
    public class InputController : MonoBehaviour, IInputController
    {
        private int m_Horizontal, m_Vertical;
        [SerializeField]
        private float m_DesiredCoolDown = .25f;
        private float m_CoolDown;

        public IntVector2 GetInput()
        {
            var returnVal = new IntVector2(m_Vertical, m_Horizontal);
            if (m_Vertical != 0 || m_Horizontal != 0)
            {
                m_CoolDown = 0f;
            }
            m_Vertical = m_Horizontal = 0;
            return returnVal;
        }

        [UsedImplicitly]
        private void Update()
        {
            m_CoolDown += Time.deltaTime;
            if (m_CoolDown < m_DesiredCoolDown)
            {
                return;
            }
            if (Input.GetButtonDown("Down"))
            {
                m_Horizontal = 0;
                m_Vertical = -1;
            }
            else if (Input.GetButtonDown("Up"))
            {
                m_Horizontal = 0;
                m_Vertical = 1;
            }
            else if (Input.GetButtonDown("Left"))
            {
                m_Horizontal = -1;
                m_Vertical = 0;
            }
            else if (Input.GetButtonDown("Right"))
            {
                m_Horizontal = 1;
                m_Vertical = 0;
            }
        }
    }
}
