﻿using JetBrains.Annotations;
using UnityEngine;

namespace Snake3D
{
    public class SnakeController : MonoBehaviour
    {
        [SerializeField] private InputController m_InputController;
        [SerializeField] private Head m_Head;
        [SerializeField] private Body m_BodyTemplate;
        [SerializeField] private float m_SpeedTime;

        // ReSharper disable once NotNullMemberIsNotInitialized
        [NotNull] private Snake m_Snake;
        private float m_CurrentTime;

        // ReSharper disable once UnusedMember.Local
        private void Awake()
        {
            if (m_Head == null || m_InputController == null || m_BodyTemplate == null)
            {
                Debug.LogError("Could not set up snake controller because input controller or head was missing");
                gameObject.SetActive(false);
                return;
            }
            m_Snake = new Snake(m_Head, m_InputController, GameManager.Instance);
            m_Snake.SetIntervalTime(m_SpeedTime);
            m_Head.RegisterCollision(HandleHeadCollision);
            m_BodyTemplate.gameObject.SetActive(false);
        }

        private void HandleHeadCollision(GameObject obj)
        {
            if (obj == null)
            {
                return;
            }
            // ReSharper disable once PossibleNullReferenceException
            switch (obj.tag)
            {
                case "Food":
                    m_Snake.AddBodyPart(m_BodyTemplate.Duplicate());
                    var food = obj.GetComponent<Food>();
                    if (food != null)
                    {
                        food.FoodEaten(m_Snake);
                    }
                    Destroy(obj);
                    break;
                case "Body":
                case "Head":
                case "Wall":
                    Debug.Log("Death!!!");
                    break;
            }
        }

        // ReSharper disable once UnusedMember.Local
        private void Update()
        {
            m_Snake.Update(Time.deltaTime);
        }
    }
}
