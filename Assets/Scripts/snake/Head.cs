﻿using System;
using JetBrains.Annotations;
using UnityEngine;

namespace Snake3D
{
    public class Head : Body, IHead
    {
        private Action<GameObject> m_OnCollision;

        [UsedImplicitly]
        private void OnTriggerEnter	(Collider col)
        {
            Debug.Log("On Collision enter " + col);
            if (col == null || m_OnCollision == null)
            {
                return;
            }
            m_OnCollision(col.gameObject);
        }

        public void RegisterCollision(Action<GameObject> onCollision)
        {
            m_OnCollision += onCollision;
        }
    }
}
