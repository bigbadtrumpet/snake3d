﻿using System;
using System.Collections.Generic;
using JetBrains.Annotations;
using UnityEngine;

namespace Snake3D
{
    public class MovementController
    {
        public MovementController([NotNull] IInputController inputController)
        {
            m_InputController = inputController;
            Rotation = Quaternion.LookRotation(Vector3.forward, Vector3.up);
        }

        [NotNull] private readonly IInputController m_InputController;

        public void ProcessMovement()
        {
            var direction = m_InputController.GetInput();
            Vector3 movement;
            if (direction.Equals(IntVector2.Zero))
            {
                movement = Rotation * new Vector3(0, 0, 1);
            }
            else
            {
                movement = Rotation * new Vector3(direction.Horizontal, direction.Vertical);
                Rotation *= Quaternion.LookRotation(new Vector3(direction.Horizontal, direction.Vertical, 0));
            }
            Position += new IntVector3(movement);
        }

        public IntVector3 Position { get; private set; }
        public Quaternion Rotation { get; private set; }
    }

    public class Snake
    {
        public Snake([NotNull] IHead head, [NotNull] IInputController inputController, [NotNull] IGameManager gameManager)
        {
            m_BodyParts = new List<BodyPosition>();
            m_MovementController = new MovementController(inputController);
            AddBodyPart(head);
            gameManager.RegisterSnake(this);
        }

        private class BodyPosition
        {
            public BodyPosition([NotNull] IBody body)
            {
                m_Body = body;
            }

            [NotNull] private readonly IBody m_Body;
            public IntVector3 Position { get; private set; }
            public Quaternion Rotation { get; private set; }

            public void SetLocation(IntVector3 position, Quaternion rotation)
            {
                Position = position;
                Rotation = rotation;
                m_Body.SetPosition(position, rotation);
            }

            public void Update(float deltaTime)
            {
                m_Body.UpdatePercentage(deltaTime);
            }
        }

        [NotNull] private readonly List<BodyPosition> m_BodyParts;
        [NotNull] private readonly MovementController m_MovementController;
        private float m_DefaultTimeInterval;
        private int m_Level;
        private float m_CurrTime;

        private float LevelTimeInternval
        {
            get { return m_DefaultTimeInterval * Mathf.Pow(.9f, m_Level); }
        }

        public void AddBodyPart(IBody bodyPart)
        {
            if (bodyPart == null)
            {
                return;
            }
            IntVector3 position;
            if(m_BodyParts.Count <= 0)
            {
                position = m_MovementController.Position;
            }
            else
            {
                // ReSharper disable once PossibleNullReferenceException
                position = m_BodyParts[m_BodyParts.Count - 1].Position + IntVector3.Back;
            }
            var newPosition = new BodyPosition(bodyPart);
            newPosition.SetLocation(position, Quaternion.identity);
            m_BodyParts.Add(newPosition);
        }

        public int BodyLength
        {
            get { return m_BodyParts.Count; }
        }

        public void ProcessMovement()
        {
            m_MovementController.ProcessMovement();
            var newPosition = m_MovementController.Position;
            var newRotation = m_MovementController.Rotation;
            foreach (var bodyPosition in m_BodyParts)
            {
                if (bodyPosition == null)
                {
                    continue;
                }
                var nextPosition = bodyPosition.Position;
                var nextRotation = bodyPosition.Rotation;
                bodyPosition.SetLocation(newPosition, newRotation);
                newRotation = nextRotation;
                newPosition = nextPosition;
            }
        }

        public void SetIntervalTime(float time)
        {
            m_DefaultTimeInterval = time;
        }

        public void IncreaseToLevel(int level)
        {
            m_Level = level;
        }

        public void Update(float deltaTime)
        {
            m_CurrTime += deltaTime;
            var percentage = m_CurrTime / LevelTimeInternval;
            for (int i = 0; i < m_BodyParts.Count; i++)
            {
                var bodyPosition = m_BodyParts[i];
                if (bodyPosition != null)
                {
                    bodyPosition.Update(percentage);
                }
            }
            if (m_CurrTime >= LevelTimeInternval)
            {
                ProcessMovement();
                m_CurrTime = 0f;
            }
        }
    }

    public interface IInputController
    {
        IntVector2 GetInput();
    }

    public interface IHead : IBody
    {
        void RegisterCollision(Action<GameObject> onCollision);
    }

    public interface IBody
    {
        void SetPosition(IntVector3 position, Quaternion rotation);
        void UpdatePercentage(float percentage);
    }
}
