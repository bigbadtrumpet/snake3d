﻿using UnityEngine;

namespace Snake3D
{
    public class Body : MonoBehaviour, IBody
    {
        private Vector3 m_DesiredPosition, m_StartPosition;
        private Quaternion m_DesiredRotation, m_StartRotation;

        public void SetPosition(IntVector3 position, Quaternion rotation)
        {
            // ReSharper disable once PossibleNullReferenceException
            m_StartPosition = transform.position;
            m_StartRotation = transform.rotation;
            m_DesiredPosition = (Vector3) position;
            m_DesiredRotation = rotation;
        }

        public void UpdatePercentage(float percentage)
        {
            // ReSharper disable once PossibleNullReferenceException
            transform.position = Vector3.Lerp(m_StartPosition, m_DesiredPosition, percentage);
            transform.rotation = Quaternion.Lerp(m_StartRotation, m_DesiredRotation, percentage * 2f);
        }
    }
}