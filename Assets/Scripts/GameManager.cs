﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace Snake3D
{
    public class GameManager : Singleton<GameManager>, IGameManager
    {
        private class SnakeInfo
        {
            public SnakeInfo(Snake snake)
            {
                Snake = snake;
            }

            public readonly Snake Snake;
            public int Score;
        }

        [SerializeField] private Bounds m_ArenaSize;
        [SerializeField] private int m_LevelUpAmount;

        private List<SnakeInfo> m_Snakes;

        public Bounds ArenaSize
        {
            get { return m_ArenaSize; }
        }

        public void RegisterSnake(Snake snake)
        {
            if (m_Snakes == null)
            {
                m_Snakes = new List<SnakeInfo>();
            }
            m_Snakes.Add(new SnakeInfo(snake));
        }

        private SnakeInfo GetSnakeInfo(Snake snake)
        {
            return m_Snakes == null
                ? null
                : m_Snakes.FirstOrDefault(snakeInfo => snakeInfo != null && snakeInfo.Snake == snake);
        }

        public void IncreaseScore(Snake snake, int foodScore)
        {
            var info = GetSnakeInfo(snake);
            if (info == null)
            {
                Debug.LogError("Could not find snake " + snake);
                return;
            }
            var previousLevel = info.Score / m_LevelUpAmount;
            info.Score += foodScore;
            var currentLevel = info.Score / m_LevelUpAmount;
            if (currentLevel > previousLevel)
            {
                info.Snake.IncreaseToLevel(currentLevel);
            }
        }
    }

    public interface IGameManager
    {
        void RegisterSnake(Snake snake);
    }
}