﻿using UnityEngine;

namespace Snake3D
{
    public class Food : MonoBehaviour
    {
        private IFoodManager m_FoodManager;
        [SerializeField] private int m_Score;

        public int Score
        {
            get { return m_Score; }
        }

        public void SetFoodManager(IFoodManager foodManager)
        {
            m_FoodManager = foodManager;
        }

        public void FoodEaten(Snake snake)
        {
            if (m_FoodManager != null)
            {
                m_FoodManager.WasEaten(this, snake);
            }
        }
    }
}

