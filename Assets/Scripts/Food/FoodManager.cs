﻿using JetBrains.Annotations;
using UnityEngine;

namespace Snake3D
{
    public class FoodManager : MonoBehaviour, IFoodManager
    {
        [SerializeField] private int m_NumberOfFood = 5;
        [SerializeField]
        private Food[] m_Foods;

        private Bounds m_Size;

        [UsedImplicitly]
        private void Awake()
        {
            if (m_Foods == null || m_Foods.Length == 0)
            {
                Debug.LogError("Need at least one model food in order for the game to work");
                enabled = false;
                return;
            }
            m_Size = GameManager.Instance.ArenaSize;
            foreach (var food in m_Foods)
            {
                food.gameObject.SetActive(false);
            }
            if (m_NumberOfFood <= 0)
            {
                Debug.LogWarning("Not enough food, will default to using 1");
                m_NumberOfFood = 1;
            }
            for (int i = 0; i < m_NumberOfFood; i++)
            {
                AddFood();
            }
        }

        private IntVector3 GetRandomPointInArena()
        {
            var x = Mathf.RoundToInt(Random.Range(-m_Size.extents.x, m_Size.extents.x) + m_Size.center.x);
            var y = Mathf.RoundToInt(Random.Range(-m_Size.extents.y, m_Size.extents.y) + m_Size.center.y);
            var z = Mathf.RoundToInt(Random.Range(-m_Size.extents.z, m_Size.extents.z) + m_Size.center.z);
            return new IntVector3(x,y,z);
        }

        private Food GetRandomFood()
        {
            // ReSharper disable once PossibleNullReferenceException
            return m_Foods[Random.Range(0, m_Foods.Length)];
        }

        private void AddFood()
        {
            var randomPoint = GetRandomPointInArena();
            var food = GetRandomFood();
            if (food == null)
            {
                Debug.LogError("Error one of the model foods is null");
                return;
            }
            food = food.Duplicate();
            food.SetFoodManager(this);
            // ReSharper disable once PossibleNullReferenceException
            food.transform.position = (Vector3) randomPoint;
        }

        public void WasEaten([NotNull] Food food, Snake snake)
        {
            AddFood();
            GameManager.Instance.IncreaseScore(snake, food.Score);
        }
    }

    public interface IFoodManager
    {
        void WasEaten(Food food, Snake snake);
    }
}
