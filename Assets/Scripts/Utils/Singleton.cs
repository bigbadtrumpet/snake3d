﻿using JetBrains.Annotations;
using UnityEngine;

namespace Snake3D
{
    public class Singleton<T> : MonoBehaviour where T : MonoBehaviour
    {
        [NotNull]
        public static T Instance
        {
            get
            {
                if (s_Instance != null)
                {
                    return s_Instance;
                }
                s_Instance = FindObjectOfType<T>();
                if (s_Instance == null)
                {
                    var go = new GameObject(typeof(T).Name);
                    s_Instance = go.AddComponent<T>();
                }
                // ReSharper disable once AssignNullToNotNullAttribute
                return s_Instance;
            }
        }
        private static T s_Instance;
    }
}