﻿using System;
using JetBrains.Annotations;
using UnityEngine;

namespace Snake3D
{
    public static class Utils
    {
        public static bool ApproxEqual(this Vector3 a, Vector3 b, float angleError = 0.1f)
        {
            //if they aren't the same length, don't bother checking the rest.
            if(!Mathf.Approximately(a.magnitude, b.magnitude))
                return false;
            var cosAngleError = Mathf.Cos(angleError * Mathf.Deg2Rad);
            //A value between -1 and 1 corresponding to the angle.
            var cosAngle = Vector3.Dot(a.normalized, b.normalized);
            //The dot product of normalized Vectors is equal to the cosine of the angle between them.
            //So the closer they are, the closer the value will be to 1.  Opposite Vectors will be -1
            //and orthogonal Vectors will be 0.
            return cosAngle >= cosAngleError;
        }

        public static bool ApproxEqual(this Quaternion a, Quaternion b, float angleError = 0.01f)
        {
            if (1f - Mathf.Abs(Quaternion.Dot(a, b)) < angleError)
            {
                return true;
            }
            return Quaternion.Angle(a, b) < angleError;
        }

        [NotNull]
        public static T Duplicate<T>(this T obj) where T : MonoBehaviour
        {
            if (obj == null)
            {
                throw new ArgumentNullException("obj");
            }
            // ReSharper disable once PossibleNullReferenceException
            var duplicate = UnityEngine.Object.Instantiate(obj, obj.transform.parent, true);
            duplicate.gameObject.SetActive(true);
            return duplicate;
        }
    }

    public struct IntVector2
    {
        public static IntVector2 Right
        {
            get { return new IntVector2(0, 1); }
        }

        public static IntVector2 Left
        {
            get { return new IntVector2(0, -1); }
        }

        public static IntVector2 Up
        {
            get { return new IntVector2(1, 0); }
        }

        public static IntVector2 Down
        {
            get { return new IntVector2(-1, 0); }
        }

        public static IntVector2 Zero
        {
            get { return new IntVector2(0, 0); }
        }

        public IntVector2(int vertical, int horizontal) : this()
        {
            Vertical = vertical;
            Horizontal = horizontal;
        }

        public IntVector2(IntVector2 currentInput) : this()
        {
            Vertical = currentInput.Vertical;
            Horizontal = currentInput.Horizontal;
        }

        public int Vertical { get; set; }
        public int Horizontal { get; set; }

        public bool HasMovement
        {
            get { return Vertical != 0 && Horizontal != 0; }
        }

        public override string ToString()
        {
            return string.Format("{0}: Horizontal, {1} Vertical", Horizontal, Vertical);
        }
    }

    public struct IntVector3
    {
        public static IntVector3 Back
        {
            get
            {
                return new IntVector3(0, 0, -1);
            }
        }

        public IntVector3(int x, int y, int z) : this()
        {
            X = x;
            Y = y;
            Z = z;
        }

        public IntVector3(Vector3 vector3) : this()
        {
            X = Mathf.RoundToInt(vector3.x);
            Y = Mathf.RoundToInt(vector3.y);
            Z = Mathf.RoundToInt(vector3.z);
        }

        public int X { get; set; }
        public int Y { get; set; }
        public int Z { get; set; }


        public override string ToString()
        {
            return string.Format("{0}: X, {1}: Y, {2}: Z", X, Y, Z);
        }

        public override bool Equals(object obj)
        {
            if (!(obj is IntVector3))
                return false;
            var intVector3 = (IntVector3) obj;
            return intVector3.X == X && intVector3.Y == Y && intVector3.Z == Z;
        }

        public override int GetHashCode()
        {
            return X.GetHashCode() ^ Y.GetHashCode() << 2 ^ Z.GetHashCode() >> 2;
        }

        public static IntVector3 operator +(IntVector3 a, IntVector3 b)
        {
            return new IntVector3(a.X + b.X, a.Y + b.Y, a.Z + b.Z);
        }

        public static IntVector3 operator -(IntVector3 a, IntVector3 b)
        {
            return new IntVector3(a.X - b.X, a.Y - b.Y, a.Z - b.Z);
        }

        public static explicit operator Vector3(IntVector3 intVector3)
        {
            return new Vector3(intVector3.X, intVector3.Y, intVector3.Z);
        }
    }
}